package se.experis;

import org.junit.Rule;
import org.junit.jupiter.api.*;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rocky;


    @BeforeEach
    void myTestSetUp() throws Exception {
        rocky = new PetRock(("Rocky"));
    }

    @Test
    void getName() throws Exception {
        assertEquals("Rocky", rocky.getName());
    }

    @Test
    void testUnappyToStart() throws Exception {
        assertFalse(rocky.isHappy());
    }

    @Test
    void testHappyAfterPlay() throws Exception {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    @Disabled("Exception throwing not yet defined")
    @Test
    void nameFail() throws Exception  {
        rocky.getHappyMessage();
    }

    @Test
    void name() throws Exception {
        rocky.playWithRock();
        String msg = rocky.getHappyMessage();
        assertEquals("I'm happy!", msg);
    }

    @Test
    void testFavNum() throws Exception {
        assertEquals(42, (rocky.getFavNumber()));
    }


    @Test
    void emptyNameFail() throws Exception {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    PetRock woofy = new PetRock("");
                });
    }
}

